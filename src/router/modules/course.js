/**
 * Created by fedrtg on 2020/8/6.
 */
/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

const courseRouter = {
  path: '/course',
  component: Layout,
  children: [
    {
      path: '',
      component: () =>
        import('@/new-views/course/courseRecord/CourseRecord.vue'),
      name: 'courseRecord',
      meta: { title: '课程记录', icon: 'table' }
    }
  ]
}
export default courseRouter
