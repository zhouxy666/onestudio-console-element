/**
 * Created by fedrtg on 2020/8/6.
 */
/** When your routing table is too long, you can split it into small modules **/

import Layout from '@/layout'

export const userRouter = {
  path: '/user',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/new-views/members/user/UserPage.vue'),
      name: 'userInfo',
      meta: { title: '学生信息', icon: 'people' }
    }
  ]
}

export const gradeRouter = {
  path: '/grade',
  component: Layout,
  children: [
    {
      path: '',
      component: () => import('@/new-views/members/grade/GradePage.vue'),
      name: 'gradeInfo',
      meta: { title: '班级信息', icon: 'peoples' }
    }
  ]
}

export const coursewareRouter = {
  path: '/courseware',
  component: Layout,
  children: [
    {
      path: 'courseware',
      component: () =>
        import('@/new-views/members/courseware/CoursewarePage.vue'),
      name: 'courseware',
      meta: { title: '课件信息', icon: 'qq' }
    }
  ]
}
