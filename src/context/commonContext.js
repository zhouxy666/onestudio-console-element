import { provide, inject } from '@vue/composition-api'

let common = Symbol()

export function useProvideHook() {
  const message = 'hello,world'

  return provide(common, {
    message
  })
}

export function useInjectHook() {
  return inject(common)
}
