import { ref, onMounted } from '@vue/composition-api'
// import { useInjectHook } from '@/context'
// import { uploadCoursewarePhoto } from '@/api/user'
export function useUploadHook(context) {
  const { $http } = context.root
  const fileList = ref([])

  onMounted(() => {
    // uploadCoursewarePhoto().then(data => {
    //   console.log(data)
    // })
  })

  function handlePreview(file, list) {
    console.log(file)
    console.log(list)
  }

  function handleRemove(file, list) {
    console.log(file)
    console.log(list)
  }

  function onSuccess(response, file, dataList){
    fileList.value = dataList
    console.log(fileList)
  }
  return { fileList, handlePreview, handleRemove, onSuccess }
}
